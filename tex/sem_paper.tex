\documentclass[a4paper, 12pt]{scrartcl}

\usepackage[utf8]{inputenc}       %%  für deutsche Umlaute und Sonderzeichen
\usepackage[english]{babel}    %%  für deutsche Umlaute und Sonderzeichen

\usepackage{amsfonts,amssymb,amsmath,amsthm, latexsym}   % einige nuetzliche Pakete
\usepackage{mathtools} % für vcentcolon

\usepackage{hyperref}   %% für Querverweise
\usepackage{url}             %% um Urls einzubinden

\usepackage{graphicx}   %%  zum Einbinden von Graphiken

% \usepackage{algorithmic}  % für Algorithmen
\usepackage{algorithm}
\usepackage{algpseudocode}

\usepackage{bbold} % for using identity matrix \mathbb{1}

\RequirePackage[tikz]{mdframed} % for left gray line
\usepackage{float} % for figure H instead of h! so it works inside framed example

% Es folgt die Festlegung der Seitenränder. Diese bitte nicht verändern!
\usepackage[top=3.5cm, bottom=3.5cm, left=2.5cm, right=2.5cm]{geometry}  

%Umgebungen definieren fuer Lemma, Theorem etc:
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}[theorem]{Corollary}
% \newtheorem{example}[theorem]{Example}
\newtheorem{remark}[theorem]{Remark}

\newtheorem{satz}[theorem]{Satz}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{korollar}[theorem]{Korollar}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{beispiel}[theorem]{Beispiel}
\newtheorem{bemerkung}[theorem]{Bemerkung}


\newmdtheoremenv[%
% outerlinewidth=2,%
% leftmargin=0,%
% innertopmargin=0,%
% rightmargin=0,%
% innerrightmargin=0,%
innertopmargin=0,%
innerbottommargin=0,%
% skipbelow=.4\baselineskip,%
% skipabove=.4\baselineskip,%
linecolor=gray,%
topline=false,%
bottomline=false,%
rightline=false,%
linewidth=2pt,%
ntheorem=true%
]{example}{Example}[section]


\newcommand{\R}{\mathbb{R}}    % Kurzbefehl für die Menge der reellen Zahlen
\newcommand{\N}{\mathbb{N}}    % Kurzbefehl für die Menge der natürlichen Zahlen
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\cQ}{\mathcal{Q}}

% Nicer looking stuff
\newcommand{\defeq}{\vcentcolon=}
\newcommand{\ie}{i.e.\@ }

% other shortcuts
\newcommand{\Ps}{\mathcal{P}_s}
\DeclareMathOperator*{\argmax}{arg\,max} % from https://tex.stackexchange.com/questions/5223/command-for-argmin-or-argmax
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator{\conv}{conv}
\DeclareMathOperator{\supp}{supp}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%       Ab hier beginnt das eigentliche LaTeX-Dokument:
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}

%%%%   Ihre Ausarbeitung MUSS eine Titelseite enthalten, die auf jeden Fall die unten genannten
%%%%   Informationen enthält.
%%%%  Die Titelseite sollte ungefähr wie folgt aussehen:

\thispagestyle{empty}    %%  keine Seitennumer auf der Titelseite!

\begin{center}

\rule{0.5\textwidth}{1.5pt} \\[3ex]
\textbf{\Huge Clusterbestimmung in Netzwerken} \\
\rule{0.5\textwidth}{1.5pt} \\

\vspace{3cm}

\begin{Large}	

\textbf{Verfasser:} 
\medskip

\textbf{Alexander Mai}

\textbf{Matrikelnummer: 1483380 }

\textbf{Studiengang: Mathematik M.Sc. }

\vspace{3cm}

\textbf{Ausarbeitung} \\
zum \\
\textbf{Seminar über Relaxierungstechniken für kombinatorische Probleme in der Netzwerkanalyse} \\[3ex]
\end{Large}

\textbf{Wintersemester 2022/23} \\
bei \\
\textbf{Prof. Dr. Mirjam Dür} \\
\textbf{Universität Augsburg} \\

\vspace{3cm}

\includegraphics[width=0.4\textwidth]{{Uni_Aug_Logo_Basis_pos_B}}  

\end{center}

%%%%   Ende der Titelseite

\newpage

\tableofcontents   %%  erzeugt an dieser Stelle ein Inhaltsverzeichnis

\newpage





\section{Introduction}

In their 2021 paper \emph{Fast cluster detection in networks by first-order optimization} \cite{ref:bomze} the authors \emph{Bomze, Rinaldi and Zeffiro} improve on previous approaches which aim to find \emph{maximal $s$-defective cliques} by reformulating this inherently discrete graph problem into a continuous maximization problem. The improved approach not only returns the cardinality of a maximal clique, but also the subset of clique vertices while avoiding so-called spurious solutions.

In this work we summarize the results of~\cite{ref:bomze} and elaborate on the most important parts in a way that can be understood with basic knowledge of optimization and graph theory and without further literature. To this end, we first summarize the ideas in the following paragraphs. Then in Chapter~\ref{sec:cliques} we introduce most of the notation used throughout this work and the concept of $s$-defective cliques. In Chapter~\ref{sec:reformulations} we explore how the discrete problem of finding cliques in a graph can be reformulated into continuous optimization problems with practically useful solutions. Finally, in Chapter~\ref{sec:algs} we get to know the \emph{Frank--Wolfe algorithm} and special variants that prove to be especially useful for our continuous reformulation of finding $s$-defective cliques.

Finding cliques in graphs started out as a problem pertaining to social networks -- be it modern social networks over the internet or graphs of people who are connected by friendship, some arbitrary kind of communication or other form of social interaction -- with its exemplary use case being crime prevention, for example by finding terrorist groups before potential attacks. %(see Figure~\ref{fig:terrorists}).

% \begin{figure}[h!]
%   \label{fig:terrorists}
%   \centering
%   \includegraphics[height=16em]{images/terrorists.pdf}
% 	\caption{caption figure used from \cite{ref:survey}.}
% \end{figure}

We can think of a clique as tightly knit group of people, so a subset of people where everyone is related to each other. In the recent decades finding maximal or maximum cliques expanded beyond sociology, one noteworthy example being medical research \cite{ref:survey}. Finding maximum cliques is an NP-hard problem that can quickly turn into brute forcing one's way through most combinations of vertices, as any single added vertex can block others from ever joining a proper clique. One simple heuristic approach that can help is removing all vertices of the graph with degree smaller than $k$, once a clique with at least $k$ vertices has been found. It is important to remember that we want to eventually handle massive graphs, especially when we look at simpler examples where most of these problems seem trivial. The cardinality of a maximal clique is often called a \emph{maximal clique number} -- note that multiple maximal clique numbers exist in any nontrivial graph -- and the unique cardinality of a maximum clique is called the \emph{maximum clique number}.

A common need in any of the use cases is \emph{relaxing} the strict tightness property of a clique to allow for missing connections or some loosely connected members in some way. The main reasoning behind this need is that often empirical data can be incomplete or erroneous, which results in missing edges or vertices of a graph. One such relaxation is that of an \emph{$s$-defective clique}, where up to $s$-many connections can be missing from a clique. Similarly to the max clique problem, efficiently finding maximal or maximum $s$-defective cliques quickly becomes unfeasible without proper algorithms, as we also have to add up to $s$ edges before looking for cliques.

An important first step is the \emph{quadratic maximal clique formulation} of \emph{Motzkin--Straus} \cite{ref:motzkinstraus}. It is a simple quadratic function with the adjacency matrix of the graph as coefficient matrix, where the global maximizers directly correspond to the maximum clique number of the graph. Unfortunately, these maximizers are in general not strict and do not directly correspond to cliques of the graph. Such maximizers are often called \emph{spurious solutions}.
%WRONG: where the global maximizer is a characteristic vector of the clique of biggest cardinality of the graph, and local maximizers correspond to maximal cliques. The global maximum value can also be directly translated into the cardinality of a maximum clique of the graph, without looking at the clique itself.

The Motzkin--Straus formulation was regularized by \emph{Bomze} in his earlier paper \emph{Evolution towards the Maximum Clique} \cite{ref:bomzeolder}, to avoid spurious solutions. In \cite{ref:bomze}, this regularization is combined with a \emph{cubic maximal $s$-defective clique formulation} -- an extension to the Motzkin--Strauss formulation, introduced in \cite{ref:sdefclique} -- which results in a \emph{regularized cubic maximal $s$-defective clique formulation}, so an objective function with isolated maximizers that directly correspond to maximal $s$-defective cliques. This resulting formulation is then thoroughly analyzed in \cite{ref:bomze} in the context of solving it using tailored variants of the Frank--Wolfe algorithm.

The Frank--Wolfe algorithm can be used to minimize convex objective functions (or maximize concave objective functions) defined on compact polytopes by iteratively generating feasible directions that lead to improved function values \cite{ref:fwalg}. It is not much more complicated than for example the Gradient Descend method, but in contrast to the latter always leads to feasible solution in each iteration for the low prize of solving a linear program.







\section{The Max $s$-Defective Clique Problem}
\label{sec:cliques}


\subsection{Notation}
Throughout this work we consider simple, undirected, unweighted graphs without multiple edges or loops. The notation here mostly adheres to the one introduced in \cite{ref:bomze} and \cite{ref:sdefclique}.

\begin{itemize}
  % \item $[a : b] \defeq \{ k \in \Z | a \le k \le b \}
  %\item $r_A = \{ r_i \in r | i \in A  \}, $ with $r \in \R$ and $A \subset [1 : d]$
  \item $e = (1, \dots, 1)^T$ the vector of ones with appropriate length, $e_i$ the $i$-th standard basis vector in $\R ^n$
  \item graph $G = (V, E), $ the number of vertices $n = | V |, $ its adjacency matrix $A_G$ and its complementary graph $\bar{G} = (V, \bar{E})$
  \item the set of \emph{``fake edge'' vectors} $D_s (G) \defeq \left \{ y \in \{ 0, 1 \}^{\bar{E}} \; | \; e^T y \le s \right \}, $ where each vector represents a unique subset of at most $s$ missing edges and the vector components are indexed with two indices each, so entry~$y_{kl} \in \{0, 1\}$ corresponds to edge~$\{ k, l \} \in \bar{E}$
  \item set of \emph{relaxed fake edge vectors} $D'_s (G) \defeq \left \{ y \in [ 0, 1 ]^{\bar{E}} \; | \; e^T y \le s \right \}, $ where now non-integer values are allowed
  \item the \emph{fake edge adjacency matrix} $A(y), $ with the same dimensions as the adjacency matrix $A_G, $ fake edges $y \in D'_s (G)$ can be relaxed, so $$ A(y)_{ij} \defeq \begin{cases}
    y_{ij}  & , \forall \{i, j\} \in \bar{E}  \\
    0       & , \forall \{i, j\} \notin \bar{E}
  \end{cases} $$
  \item $G(y)$ the \emph{augmented graph}, with adjacency matrix $A_{G(y)} = A_G + A(y),$ with an integer fake edge vector $y \in D_s (G), $ \ie a graph with up to $s$ added edges
  % \item $E(i) = ...$ the neighbors of $i$ in $G$
  % \item $E^y (i) = ...$ the neighbors of $i$ in $G(y)$
  \item $\Delta_{n-1} = \left \{ x \in [0, 1]^n \; | \; \sum_{i = 1}^n x_i = 1 \right \}$ the \emph{$(n-1)$-dimensional simplex}, where each of the $n$ vertices of the simplex represents a vertex of the graph $G$
  \item for any vertex subset $S \subseteq V$ we denote as $S$ as well the subgraph of $G$ induced by~$S, $ with edges $\{ \{i, j\} \in E \; | \; i, j \in S \}$ to avoid cluttered notation
  \item $\Delta^{(S)} = \left \{ x \in \Delta_{n-1} \; | \; x_i = 0 \ \forall i \in V \setminus S \right \}$ the \emph{$(|S|-1)$-dimensional face} of the $(n-1)$-dimensional simplex corresponding to a subset $S \subseteq V$ of vertices
  \item for any vertices $S \subseteq V$ the \emph{characteristic vector of $S$} is $x^{(S)} \in \Delta^{(S)} ,$ with $$ x^{(S)}_i = \begin{cases}
    1 / |S| & , \forall i \in S  \\
    0       & , \forall i \in V \setminus S 
  \end{cases} , $$ which is also often called the \emph{barycentre} of the simplex face $\Delta^{(S)}$
  % \item $ = \left \{  \right \}$
  % \item $ = \left \{  \right \}$
  % \item $ = \left \{  \right \}$
  % \item $ = \left \{  \right \}$
  % \item 
  % \item 
\end{itemize}



\subsection{Cliques and Missing Edges}

\begin{definition}
  A subgraph $C \subseteq V$ of a graph $G = (V, E)$ is called a \emph{clique} if it is complete, so if for every two vertices $i, j \in C,$ with $i \neq j ,$ there is an edge $\{ i, j \} \in E .$
\end{definition}

\begin{definition}
  \label{def:cliquemaximal}
  We call a clique $C$ \emph{maximal} if for all cliques $C'$ with $C \subseteq C'$ it follows that $C = C'$.
\end{definition}

Equivalently, a clique is maximal if for all vertices outside the clique $i \in V \setminus C$ the subgraph $C \cup \{ i \}$ is not a clique, so there exists a clique vertex $j \in C$ so that $\{ i, j \} \notin E .$

\begin{definition}
  \label{def:cliquemaximum}
  We call a clique $C$ \emph{maximum} if it has maximum cardinality among all cliques in a graph.
\end{definition}

Note that maximal cliques as well as maximum cliques are not unique, and maximal cliques in a graph can have different cardinality. But all maximum cliques of a graph $G$ have the same cardinality

\begin{definition}
  \label{def:cliquemaxnumber}
  The \emph{maximum clique number} $\omega(G)$ of a graph $G$ is the cardinality of a maximum clique in the graph.
\end{definition}


%VORTRAG:
% \begin{figure}[h!]
%   \centering
%   \includegraphics[height=8em]{images/example-cliques.pdf}
% 	\caption{A graph with maximum clique number $\omega (G) = 3 .$ }
%   \label{fig:exampleclique}
% \end{figure}

%The example in Figure~\ref{fig:exampleclique} has one clique $\{ 1, 2, 3 \}$ of cardinality $3$ which is maximum -- hence also maximal -- and three maximal cliques $\{ 1, 6 \}, \{ 4, 6 \}, \{ 5, 6 \} $ of cardinality $2$.

\begin{remark}
  \label{rem:inheritcliques}
  Every subgraph of a clique is again a clique, so this property is inherited. But the subgraphs of cliques can never be maximal or maximum.
\end{remark}

Remark~\ref{rem:inheritcliques} is also true for $s$-defective cliques, which we will now define.


\begin{definition}
  A subgraph $C \subseteq V$ of a graph $G = (V, E)$ is called an \emph{$s$-defective clique}, for some $s \in \N ,$ if there is a fake edge vector $y \in D_s(G)$ with $C$ a clique of the augmented graph $G(y) .$
\end{definition}

So $C$ is an $s$-defective graph of $G$ if there are up to $s$ edges that can be added to $E$ to make $C$ a clique. Or equivalently, there are at most $s$ different pairs of vertices $i \neq j \in C$ so that $\{ i, j \} \notin E .$ 

Instead of thinking of cliques with up to $s$ missing edges, it can be useful to think of $s$ edges to be added first, and then searching for cliques. Of course the resulting cliques depend on the choice of added edges. Assuming that $s \le | \bar{E} |$ and no less than $s$ edges are added, there are $\binom{| \bar{E} |}{s}$ possible edge allocations to check for cliques.

\begin{definition}
  The notion of \emph{maximal} $s$-defective cliques, \emph{maximum} $s$-defective cliques, and \emph{maximum $s$-defective clique number} -- denoted as $\omega _s (G) ,$ follows analogously to cliques. We can simply replace ``clique'' by ``$s$-defective clique'' in Definitions~\ref{def:cliquemaximal},~\ref{def:cliquemaximum} and~\ref{def:cliquemaxnumber}.
\end{definition}


\begin{example}
  \vspace*{-0.5em}

  The graph $G$ in Figure~\ref{fig:examplesdefclique} has maximum $s$-defective clique number $\omega_s (G) = 3$ for~$s \in \{ 0, 1 \} ,$ whereas~$\omega_s (G) = 4$ for~$s \in \{ 2, 3, 4 \} ,$ further~$\omega_s (G) = 5$ for $s \in \{ 5, 6, 7, 8 \}, $ and~$\omega_s (G) = 3$ for~$s \ge 9 .$

  \begin{figure}[H]
    \centering
    \includegraphics[height=6.5em]{images/example-cliques.pdf}
    \hspace*{1em}
    \includegraphics[height=6.5em]{images/example-cliques-2-def.pdf}
    \hspace*{1em}
    \includegraphics[height=6.5em]{images/example-cliques-4-def.pdf}
    \hspace*{1em}
    \includegraphics[height=6.5em]{images/example-cliques-4-def-sub.pdf}
    % \caption{The first picture shows the graph $G$ from Figure~\ref{fig:exampleclique}, the other three pictures are augmented graphs $G(y^a), G(y^b)$ and $G(y^c) ,$ with fake edge vectors $y^a, y^b, y^c$ as visualized in the pictures.}
    \caption{A graph $G$ in the first, the other three show augmented graphs $G(y^a), G(y^b)$ and $G(y^c) ,$ with fake edge vectors $y^a, y^b, y^c$ as visualized in the pictures.}
    \label{fig:examplesdefclique}
  \end{figure}
  
  Both $G(y^b)$ and $G(y^c)$ have $5$ added edges, but $\omega (G(y^c)) = 4 < 5 = \omega (G(y^b)) = \omega_5 (G) .$ This illustrates how the choice of the fake edges $y$ is a crucial step in finding maximum cliques for a given $s .$
  %VORTRAG:
  % Let us also look at $y^b, A_G$ and $A(y^b)$ for this example:

  % $$ y_{14} = y_{24} = y_{26} = y_{34} = y_{36} = 1, \quad \quad y_{15} = y_{25} = y_{35} = y_{45} = 0, $$

  % $$ A_G = \begin{pmatrix}
  %   0 & 1 & 1 & 0 & 0 & 1\\
  %   1 & 0 & 1 & 0 & 0 & 0\\
  %   1 & 1 & 0 & 0 & 0 & 0\\
  %   0 & 0 & 0 & 0 & 0 & 1\\
  %   0 & 0 & 0 & 0 & 0 & 1\\
  %   1 & 0 & 0 & 1 & 1 & 0
  %   \end{pmatrix}, \quad A(y^b) = \begin{pmatrix}
  %     0 & 0 & 0 & 1 & 0 & 0\\
  %     0 & 0 & 0 & 1 & 0 & 1\\
  %     0 & 0 & 0 & 1 & 0 & 1\\
  %     1 & 1 & 1 & 0 & 0 & 0\\
  %     0 & 0 & 0 & 0 & 0 & 0\\
  %     0 & 1 & 1 & 0 & 0 & 0
  %     \end{pmatrix}$$
  %   \vspace*{0em} % this actually does something at this place

\end{example}


The \emph{Maximum $s$-Defective Clique Problem} for a graph $G$ is the problem of finding a subset of edges $C \subseteq V$ and a fake edge vector $y \in D_s (G)$ so that $C$ is a maximum $s$-defective clique in $G$, with $y$ the corresponding up to $s$ many fake edges so that $C$ is a clique in the augmented graph $G(y) .$


\begin{remark}
  Note that an $s$-defective clique is very different from the idea of an \emph{$s$-clique}, where any vertex of the $s$-clique must be able to reach any other vertex over at most $s$ many edges. Or the idea of a \emph{$\gamma$-quasi-clique}, where the number of tolerable missing edges is relative to the number of edges in a clique of the same vertex cardinality. Or the idea of a \emph{$s$-plex}, where the degree of each vertex can be up to $s - 1$ less than it should be in a proper clique of the same vertex cardinality.
\end{remark}



\section{Reformulation as Cubic Regularized Problem}
\label{sec:reformulations}

%VORTRAG:
% For a start, it is often good to pose an optimization problem without thinking about the feasibility of solving it. For now, let us forget about $s$-defective cliques and first try to solve the maximum clique problem without any missing edges. We could formulate it as a quadratic problem like in the following, where $x \in \{ 0, 1 \}^n$ is a vector with each entry corresponding to a graph vertex, $M = ee^T - \mathbb{1}$ is the $n \times n$-dimensional adjacency matrix of a complete graph with vertex cardinality $n$.

% \begin{alignat*}{3}
%   \max_{x \in \{0, 1\}^n}              &\quad&  \sum \limits_{i = 1}^n x_i \\
%   \text{subject to: } &\quad&  x^T A_G x \,   && = x^T M x
% %                      &\quad&  x_j                       && \geq 0   &\quad \forall j=1,n
% \end{alignat*}

% The constraint gives us exactly those choices of vertices that correspond to cliques, as $0$-entries in $x$ ``remove'' the corresponding columns and rows from $A_G$ in the multiplication. Hence we only get equality in the constraint, if the $1$-entries in $x$ correspond to rows and columns (only one has to be checked, as $A_G$ is symmetric) that are $1$ everywhere but in the main diagonal. With this observation in mind, we are ready to move on to the \emph{Motzkin--Straus formulation}.



\subsection{Motzkin--Straus Quadratic Formulation for Max Clique Numbers}

\emph{Motzkin and Straus} showed in their 1965 paper \emph{Maxima for Graphs and a New Proof of a Theorem of Turán} \cite{ref:motzkinstraus} the following.


\begin{proposition}[Motzkin--Straus Quadratic Formulation]
  \label{prop:motzkin}
  Let $G = (V, E)$ be an arbitrary graph, and
  \begin{equation}\label{eq:motzkin}
    f : \Delta_{n-1} \to \R_+, \quad x \mapsto x^T A_G x ,
  \end{equation}

  then we get the following relation between the maximum clique number $\omega (G)$ and the maximum value $f^* \defeq \max_{x \in \Delta_{n-1}} f(x)$:
  \begin{equation}\label{eq:motzkinmax}
    \omega (G) = \frac{1}{1 - f^*} , \quad \quad f^* = 1 - \frac{1}{\omega (G)} .
  \end{equation}

  Further, the set of global maximizers of the objective function includes all characteristic vectors $x^{(C)}$ for maximum cliques $C$ of $G .$
\end{proposition}

In fact, Motzkin and Straus summed up over all the edges, whereas the formulation with the adjacency matrix practically does the same, but counting each edge twice, hence in their paper they get a factor of $2$ that we can save here. One practical problem is that maximizers are in general not isolated and maximizers can not always be used to identify the edges of a maximum clique.


\begin{example}
  \label{ex:motzkinspurious}
  \vspace*{-0.5em}
  Let us look at one such example with spurious solutions.

  \begin{figure}[H]
    \centering
    \includegraphics[height=3.5em]{images/example-motzkin-3-spurious.pdf}
    \caption{An example with spurious solutions for the Motzkin--Straus formulation.}
    \label{fig:examplemotzkinspurious}
  \end{figure}
  \vspace*{-1em}

  As we can quickly verify for the graph $G$ in Figure~\ref{fig:examplemotzkinspurious}, the maximum clique number is $\omega (G) = 3$ with maximum cliques $\{ 1, 2, 3 \}, \{ 1, 2, 4 \}$ and $\{ 1, 2, 5 \} .$ So using Proposition~\ref{prop:motzkin} we get that the characterstic vector of any of these maximum cliques is a global maximizer of
  $$ x^T A x = 2(x_1 x_2 + (x_1 + x_2)(x_3 + x_4 + x_5)). $$
  For $C = \{ 1, 2, 3 \}$ we get the global maximizer $x^{|C|} = (1/3, 1/3, 1/3, 0, 0)^T .$ We can now easily see, that $x^* = (1/3)(1, 1, \lambda, \mu, \nu)^T$ is a global maximizer for any $\lambda, \mu, \nu \ge 0$ with $\lambda + \mu + \nu = 1 ,$ which does not directly give us the set of vertices of a maximum clique.

\end{example}



\subsection{Regularized Quadratic Formulation for Max Cliques}

The regularized formulation from \cite{ref:bomzeolder} results in isolated maximizers that are all exactly characteristic vectors of maximal cliques, with global maximizers the characteristic vectors of maximum cliques:

\begin{equation}\label{eq:motzkinreg}
  \max_{x \in \Delta_{n-1}} x^T A_G x + \tfrac{1}{2} \| x \| ^2
\end{equation}

While we leave the proof to the avid reader of supplementary papers, we can easily verify that this makes the maximizers in the spurious solutions Example~\ref{ex:motzkinspurious} isolated. Note that, in contrast to Equation~\eqref{eq:motzkinmax}, the global maximum of the regularized formulation is $1 - (1 / 2 \omega(G)) .$

% show that is equivalent to writing it with half norm of x added to MS formulation



\subsection{Formulation for Maximum $s$-Defective Clique Number}

\begin{definition}
  We denote as $\Ps \defeq \Delta_{n-1} \times D'_s (G)$ the product domain of vertex subset vectors from $\Delta_{n-1}$ and the relaxed fake edge vectors from $D'_s(G) .$
\end{definition}

An adjusted Motzkin--Straus formulation for $s$-defective cliques was introduced by \emph{Stozhkov, Buchanan, Butenko and Boginski} in their 2020 paper \emph{Continuous cubic formulations for cluster detection problems in networks} \cite{ref:sdefclique}.

\begin{proposition}[$s$-Defective Cubic Formulation]
  \label{prop:motzkindef}
  Let $G = (V, E)$ be an arbitrary graph, and
  \begin{equation}\label{eq:motzkindef}
    f_G : \Ps \to \R_+, \quad p = (x, y) \mapsto x^T ( A_G + A(y) ) x ,
  \end{equation}

  then we get the following relation between the $s$-defective maximum clique number $\omega_s (G)$ and the maximum value:
  \begin{equation}\label{eq:motzkindefmax}
    1 - \frac{1}{\omega_s (G)} = \max \limits_{p \in \Ps} f_G (p).
  \end{equation}

  Further, the set of global maximizers of the objective function includes all $(x^{(C)}, y^{(p)}) = p \in \Ps$ with $x^{(C)}$ a characteristic vector $x^{(C)}$ for a maximum $s$-defective clique $C$ of $G ,$ and $y^{(p)} \in D_s (G)$ the corresponding fake edges to be added.
\end{proposition}

Again, this formulation allows for spurious solutions for the $x$ component of $p \ in \Ps$ as well as for the $y$ component. Note that for $y = 0$ the objective $f_G$ corresponds to the Motzkin--Straus formulation (see Equation~\eqref{eq:motzkin}).




\subsection{Regularized Formulation for Maximum $s$-Defective Clique}
\label{subsec:motzkindefreg}

Finally, in \cite{ref:bomze}, Bomze et al. combine these formulations and regularize the fake edge $y$ component of $p \in \Ps$ to get isolated maximizers with integer fake edge vectors $y \in D_s (G) .$

\begin{definition}[Regularized $s$-Defective Cubic Formulation]
  \label{def:motzkindefreg}
  Let $G = (V, E)$ be an arbitrary graph. Then the \emph{Regularized $s$-Defective Cubic Formulation} is the problem
  \begin{equation}
    \max \limits_{p \in \Ps} h_G(p) ,
  \end{equation}

  with

  \begin{equation}\label{eq:motzkindefreg}
    h_G : \Ps \to \R_+, \quad p = (x, y) \mapsto x^T ( A_G + A(y) ) x + \tfrac{\alpha}{2} \| x \| ^2 + \tfrac{\beta}{2} \| y \| ^2 ,
  \end{equation}

  with $\alpha \in (0, 2)$ and $\beta > 0 .$
\end{definition}

Note that for $y = 0$ the objective $h_G$ corresponds to the regularized quadratic formulation (see Equation~\eqref{eq:motzkinreg}).

Let us denote with $\mathcal{M}_s (G)$ the set of strict local maximizers of $h_G .$

\begin{theorem}\label{th:motzkindefreg}
  A point $p = (x, y) \in \Ps$ is in $\mathcal{M}_s (G)$ if and only if there is a maximal $s$-defective clique $C$ of $G$ so that $x = x^{(C)}$ and $y \in D_s (G)$ the corresponding fake edges to be added so that $C$ is a clique in $G(y) .$
  
  Further, the global maximizers of $h_G$ correspond to maximum $s$-defective cliques.

  Finally, for any $p \in \mathcal{M}_s (G) ,$ the objective value equates to
  \begin{equation}
    h_G (p) = 1 - \frac{2 - \alpha}{2 | C |} + s \frac{\beta}{2}
  \end{equation}
\end{theorem}


%VORTRAG:
% TODO proof:
% 1) s integer
% 2) y integer
% 3) formula at end of Prop 3.1








\section{Algorithmic Solutions}
\label{sec:algs}

\subsection{Frank--Wolfe Algorithm (FW)}

The Frank--Wolfe algorithm iteratively finds feasible directions and feasible solutions that rely on the gradient of the current iteration point. An important disadvantage of the FW algorithm is its slow convergence rate, which can be partially improved by variants such as the PARTAN (parallel tangents) direction finding method (see \cite{ref:fwalg}).

It can be used for problems $\min_{x \in \cQ} f(w) ,$ with $f \in C^1(\R^m, \R)$ convex, and $\cQ$ a nonempty bounded convex polytope, \ie $\cQ = \conv(A)$ for some $A \subseteq \R^m,\ | A | \in \N .$ % of the form $\cQ = \{ x \in \R^n \; | \; Ax \le b, x \ge 0 \} ,$ with $A \in \R^{m \times n}$ and $b \in \R^m .$
For non-convex functions it can find local minimizers.

Of course the algorithm can equivalently be used to maximize concave $C^1$ functions, or to find local maximizers of non-concave functions. We formulate the FW algorithm for the maximizing use case, as this is what we build upon in the afterwards following algorithms.

% The Frank--Wolfe algorithm is then described for this problem in Algorithm~\ref{alg:fw}.

\begin{algorithm}
  \caption{Frank--Wolfe (FW) -- for maximizing}
  \begin{algorithmic}[1]
    \State \textbf{Initialize:} Let $w_0 \in \cQ , k \defeq 0$
    \State Let $s_k \in \argmax_{y \in \cQ} \nabla f(w_k)^T y$ and $d_k \defeq s_k - w_k .$ \label{step:fdfwstart}
    \If {$\nabla f(w_k)^T d_k \le 0$}
      \State STOP, return $w_k$
    \EndIf
    \State Choose the stepsize $\alpha_k \in (0, 1]$ with a suitable criterion \label{step:stepsize}
    \State Update: $w_{k+1} \defeq w_k + \alpha_k d_k$
    \State Set $k \defeq k + 1 .$ Go to step~\ref{step:fdfwstart}.
  \end{algorithmic}
  \label{alg:fw}
\end{algorithm}

For the determination of the stepsize in algorithm step~\ref{step:stepsize}, common algorithms like exact line search or the Armijo line search algorithm can be used. The default choice for step sizes is $\alpha_k = 2 / (k + 2),\ k = 1, 2, 3, \dots ,$ or similar.


% \begin{figure}[h!]
%   \label{fig:fwjaggi}
%   \centering
%   \includegraphics[scale=0.35]{images/Frank-Wolfe_Algorithm.png}
% 	\caption{TODO caption.}
% \end{figure}
% https://commons.wikimedia.org/wiki/File:Frank-Wolfe_Algorithm.png




\subsection{Frank--Wolfe Algorithm with In Face Directions (FDFW)}

Denote with $\mathcal{F}(w)$ the minimal face of the convex bounded polytope $\cQ$ containing $w \in \cQ .$ This FW variant described in Algorithm~\ref{alg:fdfw} chooses between a classic FW direction $d_k^{\mathcal{FW}}$ and an in face direction $d_k^{\mathcal{FD}} ,$ depending on the directions' scalar product with the current gradient. While the classic direction points towards the vertex maximizing the scalar product with the gradient, the in face direction is bounded to the current minimal face containing $w_k$ and is chosen as the negative of the direction that minimizes the scalar product with the gradient. In face steps drop the dimension of the current minimal face containing $w_k$ by one if the step size is maximal, which results in good convergence properties. This leads to Algorithm~\ref{alg:fdfw}.

\begin{algorithm}
  \caption{Frank--Wolfe with In Face Directions (FDFW)}
  \begin{algorithmic}[1]
    \State \textbf{Initialize:} $w_0 \in \cQ, k \defeq 0$
    \State Let $s_k \in \argmax_{y \in \cQ} \nabla f(w_k)^T y$ and $d_k^{\mathcal{FW}} \defeq s_k - w_k .$ \label{step:fdfwstart}
    \If {$w_k$ is stationary}
      \State STOP, return $w_k$
    \EndIf
    \State Let $v_k \in \argmin_{y \in \mathcal{F}(w_k)} \nabla f(w_k)^T y$ and $d_k^{\mathcal{FD}} \defeq w_k - v_k$
    \If {$\nabla f(w_k)^T d_k^{\mathcal{FW}} \ge \nabla f(w_k)^T d_k^{\mathcal{FD}}$}
      \State $d_k \defeq d_k^{\mathcal{FW}}$
    \Else
      \State $d_k \defeq d_k^{\mathcal{FD}}$
    \EndIf
    \State Choose the stepsize $\alpha_k \in (0, \alpha_k^{\max}]$ with a suitable criterion
    \State Update: $w_{k+1} \defeq w_k + \alpha_k d_k$
    \State Set $k \defeq k + 1 .$ Go to step~\ref{step:fdfwstart}.
  \end{algorithmic}
  \label{alg:fdfw}
\end{algorithm}

In the Paper~\cite{ref:bomze}, the authors go on to prove general results of the FDFW algorithm. We will skip straight to the most important result that is relevant to our problem of $s$-defective cliques and then to the variant proposed for the problem we are interested in and the result for it.


Consider the Condition~\ref{cond:s1} for a lower bound on the stepsize for some $c > 0 ,$ and the Condition~\ref{cond:s2} for a sufficient increase in the objective value for some $\rho > 0$.
\begin{equation*}\tag{S1}\label{cond:s1}
  \alpha_k \ge \widetilde{\alpha}_k \defeq \min ( \alpha_k^{\max}, c \dfrac{\nabla f(w_k)^T d_k}{\| d_k \| ^2} )
\end{equation*}

\begin{equation*}\tag{S2}\label{cond:s2}
  f(w_k + \alpha_k d_k) - f(w_k) \ge \rho \widetilde{\alpha}_k \nabla f(w_k)^T d_k
\end{equation*}

For the following Proposition we apply the FDFW to the regularized $s$-defective cubic formulation (see Chapter~\ref{subsec:motzkindefreg}).

\begin{proposition}(from \cite[Corollary 4.2]{ref:bomze})\label{prop:fdfwflobal}
  Let $\{ z_k \}$ be a sequence generated by the FDWF and assume that at least one limit point $p = (x^{(C)}, y^{(p)})$ of $\{ z_k \}$ is in $\mathcal{M}_s(G) .$ Then under the conditions~\ref{cond:s1} and~\ref{cond:s2} on the stepsize we have $ z_k \to p$ with $\supp(x_k) \subset C$ and $y_k = y_p$ for k large enough.
\end{proposition}

To put it simply, with Proposition~\ref{prop:fdfwflobal} we know that if we apply the FDFW to our regularized $s$-defective cubic formulation and the algorithm does not get stuck on a stationary point that is no maximizer of our problem, \ie not an element of$\mathcal{M}_s (G) ,$ then the algorithm will necessarily converge to a maximizer and from some finite $k$ large enough onwards, the fake edge vector component $y_k$ will already be the optimal vector of fake edges for the $x^{(C)}$ that $x_k$ is not only converging to. In fact, the support of $x_k$ also eventually is a subset of the support of $x^{(C)} ,$ so we already know that we are not choosing vertices anymore, that will not be in the final subset of vertices that mae up our maximal clique.



\subsection{Tailored Frank--Wolfe Algorithm (FWdc)}

Based on their general results on the FDFW algorithm, Bomze et al. propose a variant of the FW algorithm specifically suited to the regularized $s$-defective cubic formulation (see Chapter~\ref{subsec:motzkindefreg}). This variant of the FW algorithm changes the vertex subset component $x$ of $p = (x, y) \in \Ps$ and its fake edge vertex component $y$ alternatingly. For the $x$ component the classic FDFW step ist used, while for the $y$ component a full classic FW step is used, so with stepsize $\alpha_k = 1$. This leads to $y$ always staying an integer vector $y \in D_s (G) ,$ which leads to Algorithm~\ref{alg:fwdc}.

\begin{algorithm}
  \caption{Frank--Wolfe for $s$-Defective Clique (FWdc)}
  \begin{algorithmic}[1]
    \State \textbf{Initialize:} $z_0 \defeq (x_0, y_0) \in \Ps , k \defeq 0$
    \If {$z_k$ is stationary} \label{step:fwdcstart}
      \State STOP
    \EndIf
    \State Compute $x_{k+1}$ applying one iterate of Algorithm~\ref{alg:fdfw} with $w_0 = x_k$ and $f(w) = h_G(w, y_k) .$
    \State Let $y_{k+1} \in \argmax_{y \in D'_s (G)} \nabla_y h_G (x_k, y_k)^T y .$
    \State Set $k \defeq k + 1 .$ Go to step \ref{step:fwdcstart}.
  \end{algorithmic}
  \label{alg:fwdc}
\end{algorithm}


For this algorithm applied to our regularized $s$-defective cubic formulationm the $y$ component only changes a finite amount of times during the algorithm, to be exact at most
\begin{equation}\label{eq:ysteps}
  \frac{2}{\beta} - \frac{2 - \alpha}{\beta | \omega_s(G) |} + s
\end{equation}
times (see \cite[Proposition 5.1]{ref:bomze}).

Finally, from the result~\ref{eq:ysteps}, referencing another paper, it follows that if Conditions~\ref{cond:s1} and~\ref{cond:s2} hold, the FWdc algorithm



\subsection{Empirical Results}

In the appendix of their paper, numerical results are presented for the application of the FWdc compared to the FDFW and a solver from another paper, called CONOPT. The results show clear advantages of the FWdc compared to the other to in terms of running time -- a factor of around $10 ^{-2} ,$ as can be seen in Figure~\ref{fig:numbox}. And in terms of the cardinality of the cliques found, the FWdc is at least as good as the other two, with a notable difference in the derivation of the results of the multiple runs, which is considerably larger for the other two algorithms.


\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{images/numeric-box.pdf}
  \caption{Box graphs from the Paper~\cite{ref:bomze}. Here the regularization parameters were set to $\alpha = 1$ and $\beta = 2 / n^2 .$ The algorithms were tested on graph instances taken from the Second DIMACS Implementation Challenge as representative benchmark graphs. Each column represents data points of one of the algorithms applied on all 50 challenge graphs, with $s$ specified by the number next to the algorithm name on the $x$-axis of the plot. The boxes correspond to the data between the 25th and 75th percentile of the results.}
  \label{fig:numbox}
\end{figure}









%%%%%
%%   Das Lteraturverzeichnis kann mit BibTeX erstellt werden. Fuer Anfaenger ist es einfacher, 
%%   das Verzeichnis "von Hand" zu erstellen, wie unten dargestellt. Beide Varianten sind zulaessig.
%%%%%

\newpage

\begin{thebibliography}{9}
\addcontentsline{toc}{section}{References}   %%  Aufnahme ins Inhaltsverzeichnis

\bibitem{ref:bomze} % oben referenzierbar mit \cite{ref:bomze}
  I.M. Bomze, F. Rinaldi, and D. Zeffiro:
  \emph{Fast cluster detection in networks by first-order optimization}.
  arXiv-Preprint No. 2103.15907v1 (2021).

\bibitem{ref:survey} % [26]
  J. Pattillo, N. Youssef, and S. Butenko:
  \emph{On clique relaxation models in network analysis}. 
  European Journal of Operational Research, 226(1):9–18 (2013).

\bibitem{ref:motzkinstraus}
  T.S. Motzkin, and E.S. Straus:
  \emph{Maxima for Graphs and a New Proof of a Theorem of Turán}. 
  Canadian Journal of Mathematics, 17, 533-540. doi:10.4153/CJM-1965-053-6 (1965).

\bibitem{ref:bomzeolder} % [4]
  I.M. Bomze:
  \emph{Evolution towards the maximum clique}. 
  Journal of Global Optimization, 10(2):143–164 (1997).

\bibitem{ref:sdefclique} % [28]
  V. Stozhkov, A. Buchanan, S. Butenko, and V. Boginski:
  \emph{Continuous cubic formulations for cluster detection problems in networks}.
  Mathematical Programming, online (2020).

\bibitem{ref:fwalg}
  S. Lawphongpanich:
  \emph{Frank–Wolfe Algorithm}.
  In: C. Floudas, P. Pardalos. (eds)
  \emph{Encyclopedia of Optimization}.
  Springer, Boston, MA. \url{https://doi.org/10.1007/978-0-387-74759-0_191} (2008).

% \bibitem{Beu1999}
%   Albrecht Beutelspacher:
%   \emph{Das ist o.B.d.A. trivial!} 
%   Neunte, aktualisierte Auflage. Wiesbaden, Vieweg + Teubner, 2009.

% \bibitem{Zitierleitfaden}
% Fachschaft Mathematik, Universität Augsburg: 
% \emph{Leitfaden zum Thema Zitieren in mathematischen Arbeiten} (2017).
% Downloadbar unter:  \url{http://www.student.uni-augsburg.de/de/fachschaften/mathe/downloads/}

\end{thebibliography}

\end{document}			% Ende des Dokuments
